const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8000;

app.use(express.static(__dirname + "/views"))

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/index.html"))
})

app.get("/cart", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/shopping-cart.html"))
})

app.get("/foods", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/products.html"))
})

app.get("/admin", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/admin/indexAdmin.html"))
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})